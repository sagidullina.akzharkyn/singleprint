const express        = require('express');
const app            = express();
const bodyParser = require('body-parser')
const fs = require('fs')
const { crc16ccitt } = require('crc');
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest
const xhr = new XMLHttpRequest()
const port = 3000

let label;
var a = "^XA FO55,120 TEST ^XZ"

const urlencodedParser = bodyParser.urlencoded({extended: false})
app.use(express.static(__dirname+'/public'))

app.get("/", urlencodedParser, function (request, response) {
    response.sendFile(__dirname + "/public/index.html");
});
app.post("/registration", urlencodedParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    var merchant = {
    	inputmMpan: request.body.inputmMpan,
    	inputMcc: request.body.inputMcc,
    	inputName: request.body.inputName,
        inputCity: request.body.inputCity,
        inputCopy: request.body.inputCopy
    }
    var padName = merchant.inputName.length.toString().padStart(2, '0')
    var padCity = merchant.inputCity.length.toString().padStart(2, '0')

    var plwithoutcrc = `0002010216${merchant.inputmMpan}5204${merchant.inputMcc}53033985802KZ59${padName}${merchant.inputName}60${padCity}${merchant.inputCity}6304`

    var crc = (crc16ccitt(plwithoutcrc).toString(16)).toUpperCase()
    var plwithcrc = `${plwithoutcrc}${crc}`
    var xxxx = plwithcrc.length.toString().padStart(4, '0')
    var qrpayload = `MM,B${xxxx}\n${plwithoutcrc}${crc}`
    label = `^XA\n
            \n^FO35,80^GFA,4260,4260,15,,::::::::::::::::::::::::::::::::::::::::::::::::S01LF8,S03LF8,:S01LF8,V03F,W0F,W07,W078,W038,W03C,:W07C,W0FC,S01JF8,S03JF8,S03JF,S01IFE,T0FFE,,::U018,U0FF,T03FFC,T07FFE,T0JF,S01FC3F,S01F00F8,S01E0078,S03E007C,S03C003C,:::S03E007C,S01E0078,S01F00F8,T0FC3F,T0JF,O06J07FFE,M018F8I03FFC,M019F8J0FF,M0199C,:M01B98,M01F18,N0E,S03JF8,::N018I01JF8,N07EL07E,N0FFM0F,M01C38L078,M01818L078,M0181CL038,M0181CL03C,M01818L07C,M01818L0FC,S01JFC,S03JF8,S03JF,S01IFE,V0FE,M01CM01F,M01FN0F,N07EM078,N07F8L038,N0678L03C,N06F8L03C,N07EM07C,N0F8L03FC,M01EJ03JF8,M018J03JF8,S03JF,S01IFC,,N0FF,M01FF8,:O0FK07E,N01EJ01FFC,N03CJ07FFE,N078J0JF,N0FK0JF8,M01FF8001F9EF8,M01FF8001E1C78,S03E1C3C,S03C1C3C,:::S03C1E78,S03E1FF8,S01E1FF,:T0E1FC,N0EM0F,M01FF,M019F8,M019DC,M01FDC,M01FF8001LF,M01E78003LF8,M01EJ03LF8,N03J03LF8,S01LF8,T0F81F,S01E0078,:S03C0038,S03C003C,::S03E007C,S01F00F8,M01FF8001FC3F8,M01FFCI0JF,N031CI07IF,N031CI03FFC,N031CJ0FF,N03B8,N01F8,O0F,T03C,T0FE,M018J01FF03,M01EJ01FF878,N0F8I03FF878,N07FI03E3C38,N0678003C1C3C,N067800381C3C,N07F8003C1C3C,N07EI01C1C3C,M01FK0E1C3C,M01CK0F1E78,S01JF8,S03JF8,P08003JF,O038003IFE,O078001IF8,N01E,M01FC,:O0F,O038,O018001JF8,S03JF8,:S01JF8,T0JF,V01F,W0F,W078,W038,W03C,:W07C,:S01JFC,S03JF8,:S03JF,S01IF8,,::::S01LF8,S03LF8,::T0KFE,U01E,:U03E,U07F8,T01FFC,T03IF,T0FF3F8,S01FC1F8,S03F8078,S03E0038,S03CI08,S01,,::::::::::::::::::::::::::::::::::::::::::::::::^FS
            \n^FO57,379^GFA,1071,1071,9,JFN03IFC,::::::::::::::::::JFM083IFC,JFL0183IFC,::JFL0383IFC,:JFL0783IFC,JFK01F83IFC,JFK0FF83IFC,JFJ0IF83IFC,JFI0JF83IFC,JF007JF03IFC,JF03KF03IFC,JF03JFC03IFC,JF03IF8003IFC,JF03FFJ03IFC,JF03FCJ03IFC,JF03FFJ03IFC,JF03FFEI03IFC,JF01IF8003IFC,JF007IF003IFC,JF001IFC03IFC,JFI07IF03IFC,JFJ0IF83IFC,JFJ03FF83IFC,JFK0FF83IFC,JFK03F83IFC,JF038I0783IFC,JF03FC00183IFC,JF03FFCI03IFC,JF03IFC003IFC,JF03JFE03IFC,JF03KF83IFC,JF00KF83IFC,JFI0JF83IFC,JFJ0IF83IFC,JFK07F83IFC,JFL0783IFC,JF03EK03IFC,:JF03E01F003IFC,JF03E03F803IFC,JF07C07FC03IFC,JF07C0FFE03IFC,JF07C0IF03IFC,JF07C1IF03IFC,:JF03E3F9F83IFC,JF03E7F0F83IFC,JF03IF0F83IFC,JF03FFE0F83IFC,JF01FFE0F83IFC,JF00FFC0F83IFC,JF00FF80F83IFC,JF003F01F83IFC,JF02J0F03IFC,JF038K03IFC,JF03EK03IFC,JF03F8J03IFC,JF03FFJ03IFC,JF03FFCI03IFC,JF03IFI03IFC,JF00IFC003IFC,JF007IF803IFC,JF007IFE03IFC,JF007CIF03IFC,JF007C3FF03IFC,JF007C07F83IFC,:JF007CIF83IFC,JF007JF83IFC,JF01KF83IFC,JF03KF03IFC,JF03JF003IFC,JF03IFI03IFC,JF03FFJ03IFC,JF03EK03IFC,JF02L03IFC,JFN03IFC,:::::::::::::::::::::^FS
            \n^FO608,100\n^FB442,2,0,C\n^ASR,12,14\n^FD${merchant.inputName}\n^FS
            \n^FO245,122^BQN,2,9\n^FD\n${qrpayload}\n^FS
            \n^FO170,143\n^ATR,18,19^FD\n${merchant.inputmMpan.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/,'')}\n^FS
            \n^PQ${merchant.inputCopy},N
            \n^XZ\n`
    fs.writeFile('qrsticker.lbl', label, (err) => {  
        if (err) throw err;
        console.log('Lyric saved!');
    });  
    //console.log(`MM,B${xxxx}\n${plwithoutcrc}${crc}`) 
    var data = JSON.stringify({
        device: {
            name: "18j183203678",
            uid: "18j183203678",
            connection: "usb",
            deviceType: "printer",
            version: 2,
            provider: "com.zebra.ds.webdriver.desktop.provider.DefaultDeviceProvider",
            manufacturer:"Zebra Technologies"
        },
        data:label
    
    });
    xhr.open('POST','http://127.0.0.1:9100/write', false)
    xhr.setRequestHeader('Content-Type', 'text-plain');
    xhr.send(data) 
    try {
        fs.unlinkSync(__dirname+'/qrsticker.lbl')
        //file removed
      } catch(err) {
        console.error(err)
      } 
    response.send(qrpayload)
    //response.sendFile(__dirname + '/public/index.html')

});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})
